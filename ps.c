// This file was edited by Kfir Levy, ID 213379258
// The file is a user-space program that uses the kernel-space cps158 function.
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int
main(int argc, char *argv[])
{
    cps158();
    exit();
}